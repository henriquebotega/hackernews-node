export const typeDefs = `
    type Car {
        id: ID!
        name: String!
        color: String!
    }

    type Query {
        getAll: [Car!]!
        getByID(id: ID!): Car
    }

    type Mutation {
        insertCar(name: String!, color: String!): Car!

        updateCar(id: ID!, name: String!, color: String!): Car

        deleteCar(id: ID!): Car
    }
`;
