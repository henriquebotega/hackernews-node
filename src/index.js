import { ApolloServer } from "apollo-server";
import { makeAugmentedSchema } from "neo4j-graphql-js";
import neo4j from "neo4j-driver";

import { typeDefs } from "./schema.js";

const driver = neo4j.driver(
    "bolt://localhost:7687",
    neo4j.auth.basic("neo4j", "rentcar")
);

const resolvers = {
    Query: {
        getAll: async () => {
            const session = driver.session();
            const cypher = "MATCH (c: Car) RETURN c";

            try {
                const result = await session.run(cypher);

                return result.records.map((r) => {
                    return {
                        id: r.get(0).identity.toString(),
                        ...r.get(0).properties,
                    };
                });
            } catch (e) {
                console.log(e);
            } finally {
                await session.close();
            }
        },
        getByID: async (_, args) => {
            const session = driver.session();
            const cypher = `MATCH (c: Car) WHERE id(c) = ${args.id} RETURN c`;

            try {
                const result = await session.run(cypher);
                const r = result.records[0];

                return {
                    id: r.get(0).identity.toString(),
                    ...r.get(0).properties,
                };
            } catch (e) {
                console.log(e);
            } finally {
                await session.close();
            }
        },
    },
    Mutation: {
        insertCar: async (_, args) => {
            const session = driver.session();
            const cypher = `CREATE (c:Car {name: '${args.name}', color: '${args.color}'}) RETURN c`;

            try {
                const result = await session.run(cypher);
                const r = result.records[0];

                return {
                    id: r.get(0).identity.toString(),
                    ...r.get(0).properties,
                };
            } catch (e) {
                console.log(e);
            } finally {
                await session.close();
            }
        },
        updateCar: async (_, args) => {
            const session = driver.session();
            const cypher = `MATCH (c: Car) WHERE id(c) = ${args.id} RETURN c`;

            try {
                const result = await session.run(cypher);

                if (result.records.length > 0) {
                    const r = result.records[0].get(0);
                    const id = r.identity.toString();

                    const cypherEdit = `MATCH (c: Car) WHERE id(c) = ${id} SET c.name = '${args.name}', c.color = '${args.color}' RETURN c`;
                    const resultEdit = await session.run(cypherEdit);

                    const rEdit = resultEdit.records[0].get(0);

                    return {
                        id,
                        ...rEdit.properties,
                    };
                }
            } catch (e) {
                console.log(e);
            } finally {
                await session.close();
            }
        },
        deleteCar: async (_, args) => {
            const session = driver.session();
            const cypher = `MATCH (c: Car) WHERE id(c) = ${args.id} RETURN c`;

            try {
                const result = await session.run(cypher);

                if (result.records.length > 0) {
                    const r = result.records[0].get(0);
                    const id = r.identity.toString();

                    const cypherDel = `MATCH (c: Car) WHERE id(c) = ${id} DETACH DELETE c`;
                    await session.run(cypherDel);

                    return {
                        id,
                        ...r.properties,
                    };
                }
            } catch (e) {
                console.log(e);
            } finally {
                await session.close();
            }
        },
    },
};

const schema = makeAugmentedSchema({ typeDefs, resolvers });

const server = new ApolloServer({
    schema,
    context: { driver },
});

server
    .listen(3003)
    .then(({ url }) => console.log(`Server is running on ${url}`));
